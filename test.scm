(import (scheme base)
        (scheme write)
        (retropikzel kv v0.1.0 main))


(define kv1 (kv-make '((a . 1)
                       (b . 2)
                       (c . 3)
                       (d . 4))))

(display "kv1: ")
(write (kv-data kv1))
(newline)

(display "kv1, key a: ")
(display (kv-get kv1 'a))
(newline)


(define kv2 (kv-make '((a . 1) (b . 2))))

(display "kv2, key a: ")
(write (kv-get kv2 'a))
(newline)

(display "kv2: ")
(write (kv-data kv2))
(newline)

(define kv3 (kv-merge kv2 '((a . 3))))

(display "kv2, key a: ")
(write (kv-get kv2 'a))
(newline)

(display "kv2: ")
(write (kv-data kv2))
(newline)

(display "kv3, key a: ")
(write (kv-get kv3 'a))
(newline)

(display "kv3: ")
(write (kv-data kv3))
(newline)


(define kv4 (kv-merge kv3 '((c . 3))))

(display "kv4, key c: ")
(write (kv-get kv4 'c))
(newline)

(display "kv4: ")
(write (kv-data kv4))
(newline)


(define kv5 (kv-merge kv4 '((c . 6) (d . 8))))

(display "kv5, d: ")
(write (kv-get kv5 'd))
(newline)

(display "kv5: ")
(write (kv-data kv5))
(newline)


(define kv6 (kv-merge kv4 kv2))

(display "kv6: ")
(write (kv-data kv6))
(newline)


(display "kv6, key not in there: ")
(write (kv-get kv6 'foobar))
(newline)

(define kv7 (kv-make '((a . (1 2 3 4))
                       (b . "bar"))))
(display "kv7, key a: ")
(write (kv-get kv7 'a))
(newline)
(display "kv7, key b: ")
(write (kv-get kv7 'b))
(newline)
(display "kv7, key a replace: ")
(write (kv-get (kv-merge kv7 '((a . "foo"))) '()))
(newline)


(display "kv7, keys: ")
(write (kv-keys kv7))
(newline)

(display "kv7, values: ")
(write (kv-values kv7))
(newline)

(display "kv7 has key a?: ")
(write (kv-has-key? kv7 'a))
(newline)

(display "kv7 has key z?: ")
(write (kv-has-key? kv7 'z))
(newline)

(display "kv7 first item is: ")
(write (kv-nth kv7 0))
(newline)

