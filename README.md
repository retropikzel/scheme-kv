Immutable data structure made to be comfortable to use.

    (define kv1 (kv-make '((a . 1) (b . 2))))
    (kv-get kv1 'a)
    > 1
    (kv-data kv1)
    > ((a . 1) (b . 1))

    (define kv2 (kv-merge kv1 '((c . 3))))
    (kv-get kv2 'a)
    > 1
    (kv-data kv2)
    > ((a . 1) (b . 2) (c . 3))
    (kv-get kv1 'c)
    > ()

