(define-library
  (retropikzel kv v0.1.0 main)
  (import (scheme base)
          (scheme write))
  (export kv-make
          kv?
          kv-get
          kv-data
          kv-merge
          kv-keys
          kv-values
          kv-has-key?
          kv-nth)
  (begin

    (define-record-type kv-type (kv-record data) kv? (data kv-data))

    (define kv-make
      (lambda (alist)
        (kv-record alist)))

    (define kv-get
      (lambda (kv key)
        (if (null? key)
          (kv-data kv)
          (let ((item (assoc key (kv-data kv))))
            (if item
              (cdr item)
              '())))))

    (define kv-update-or-append
      (lambda (data key value)
        (let* ((found? #f)
               (result (list)))
          (for-each
            (lambda (p)
              (if (eq? (car p) key)
                (begin
                  (set! found? #t)
                  (set! result
                    (append result (list (cons (car p) value)))))
                (set! result
                  (append result (list p)))))
            data)
          (if found?
            result
            (append result (list (cons key value)))))))

    (define kv-merge
      (lambda (kv1 kv2)
        (let ((result (if (kv? kv1) (kv-data kv1) kv1)))
          (for-each
            (lambda (p2)
              (set! result (kv-update-or-append result (car p2) (cdr p2))))
            (if (kv? kv2) (kv-data kv2) kv2))
          (kv-make result))))

    (define kv-make-old
      (lambda (alist)
        (letrec* ((procedure
                    (lambda (key value data)
                      (cond ((and (symbol? key)
                                  (null? value))
                             (let ((p (assoc key data)))
                               (if p
                                 (cdr p)
                                 '())))
                            (else
                              (let* ((data (if (symbol? key)
                                             (kv-update-or-append data key value)
                                             (kv-update-or-append-list data key))))
                                (if (and (null? key)
                                         (null? value))
                                  data
                                  (lambda (key . value)
                                    (procedure key value data)))))))))
          (lambda (key . value)
            (procedure key value alist)))))

    (define kv-keys
      (lambda (kv)
        (map car (kv-data kv))))

    (define kv-values
      (lambda (kv)
        (map cdr (kv-data kv))))

    (define kv-length
      (lambda (kv)
        (length (kv-data kv))))

    (define kv-has-key?
      (lambda (kv key)
        (if (assoc key (kv-data kv)) #t #f)))

    (define kv-nth
      (lambda (kv index)
        (if (>= (kv-length kv) index)
          (list-ref (kv-data kv) index)
          '())))


    ))
